<?= $this->load->view('includes/template/header') ?>
<header class="intro  flex-items-xs-middle  parallax" 
        data-stellar-background-ratio="0.5"
        data-stellar-vertical-offset="300"
        data-stellar-offset-parent="true"
        style="background-image: url('<?= $detail->foto ?>');">

        <div class="pattern" style="opacity: 0.25;"></div>

        <div class="container">
                <div class="intro__text">
                        <p class="intro__subtitle"><?= $detail->subtitulo ?></p>
                        <h1 class="intro__title"><?= $detail->titulo ?></h1>
                        <p class="intro__post-date"><?= ucfirst(strftime("%a",strtotime($detail->fecha))); ?>,<?= strftime("%d-%m-%Y",strtotime($detail->fecha)); ?></p>
                </div>
        </div>
</header>

<main role="main">
    <!-- start section -->
    <section class="section">
        <div class="container">
            <div class="blog single-content">
                <div class="row flex-items-md-center">
                    <div class="col-md-10">
                        <?= $detail->texto ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
    <?php $this->load->view('includes/template/contacto'); ?>
</main>