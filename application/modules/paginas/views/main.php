<?= $this->load->view('includes/template/header') ?>
<header id="start-screen" class="start-screen--style-1">
    <?= $this->load->view('includes/template/menu') ?>
    <?= $this->load->view('includes/template/banner') ?>
</header>
<main role="main"> 
    <!-- start section -->
    <section class="section section--no-pt section--no-pb">
        <div class="container-fluid">
            <div class="products--style-2">
                <div class="products__inner">
                    <div class="row row-no-gutter">
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="product__item">
                                <figure>
                                    <img src="http://airfoc.cat//img/blank.gif" style="background-image: url('http://airfoc.cat//img/gall_img/2_col/6.jpg');" alt="demo">
                                </figure>
                                <div class="product__item__description">
                                        <div class="product__item__description__inner">
                                            <h2 class="product__item__title">enginyeria de seguretat</h2>
                                            Assessorament tècnic, Plans d'evacuació,
                                        </div>
                                        <div class="product__item__description__inner">Cursos de formació.</div>                                    
                                </div>                                
                            </div>                                                
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="product__item">
                                <figure>
                                    <img src="http://airfoc.cat//img/blank.gif" style="background-image: url('http://airfoc.cat//img/gall_img/4_col/15.jpg');" alt="demo" data-mce-style="background-image: url('http://airfoc.cat//img/gall_img/4_col/15.jpg');">
                                </figure>
                                <div class="product__item__description">
                                    <div class="product__item__description__inner">
                                        <h2 class="product__item__title">
                                            INSTAL.LACIÓ<br> D'EXTINTORS
                                        </h2>
                                        Extintors d'incendis, BIE 25/45 + accessoris, Revisions periòdiques, Càrregues d'extintor.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <div class="product__item">
                                <figure>
                                    <img src="http://airfoc.cat//img/blank.gif" style="background-image: url('http://airfoc.cat//img/gall_img/4_col/10.jpg');" alt="demo">
                                </figure>
                                <div class="product__item__description">
                                    <div class="product__item__description__inner">
                                        <h2 class="product__item__title">senyalització<br>per A incendis</h2>
                                        Senyals, pictogrames especials, pintura, etc.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section --> <!-- start section --> 
    <a id="serveis" class="ancor mce-item-anchor"></a>
    <section class="section section--screen section-services">
        <div class="container">
            <div class="row">
                <div class="col-xl-4">
                    <div class="col-MB-30">
                        <h2>Airfoc<br> Serveis</h2>
                        <h4>Per tal d'adequar cada element extintor a la normativa vigent, s'ofereix un servei de manteniment d'extintors, instal·lacions i material de protecció contra incendis en general.</h4>
                        <p>Més de 30 anys ens avalen en el sector.</p>
                    </div>
                </div>
                <div class="col-xl-8">
                    <div class="feature feature--style-3">
                        <div class="feature__inner">
                            <div class="row flex-items-xs-between"><!-- start item -->
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="feature__item">
                                        <i class="feature__item__ico feature__item__ico--1"></i>
                                        <h3 class="feature__item__title h4">CONTRACTES DE MANTENIMENT D'EQUIPAMENT I D'INSTAL.LACIONS</h3>
                                        <p>Per a l'obtenció de permisos d'activitats i pàrquings és imprescindible disposar del contracte de manteniment dels sistemes de protecció contra incendis amb una empresa autoritzada.</p>                                        
                                    </div>
                                </div><!-- end item --> <!-- start item -->
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="feature__item">
                                        <i class="feature__item__ico feature__item__ico--2"></i>
                                        <h3 class="feature__item__title h4">REVISIONS PERIÒDIQUES EN TOTS ELS PRODUCTES<br></h3>
                                        <p>Cal realitzar una revisió periòdica dels equips i sistemes de protecció contra incendis amb l'objectiu de verificar el seu correcte funcionament i instal·lació. Així mateix, es verifica el compliment de la normativa en cada moment.</p>                                        
                                    </div>
                                </div><!-- end item --> <!-- start item -->
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="feature__item"><i class="feature__item__ico feature__item__ico--3"></i>
                                        <h3 class="feature__item__title h4">CÀRREQUES D'EXTITORS AMB SUBSTITUCIÓ INMEDIATA<br></h3>
                                        <p>El servei de càrrega d'extintors d'incendis és obligatori per a tots els extintors que s'hagin utilitzat o l’hagin perduda, així com per aquells en què hagi caducat.</p>                                        
                                    </div>
                                </div><!-- end item --> <!-- start item -->
                                <div class="col-xs-12 col-sm-6 col-lg-3">
                                    <div class="feature__item"><i class="feature__item__ico feature__item__ico--4"></i>
                                        <h3 class="feature__item__title h4">NORMÀTIVA EN MATÈRIA DE PROTECCIÓ CONTRA INCENDIS&nbsp;<br></h3>
                                        <p>
                                            Els serveis tècnics de MAIFOC es mantenen actualitzats en la normativa corresponent en matèria de protecció contra incendis. Aquests coneixements es transmeten a cada client mitjançant els contractes de manteniment, les revisions periòdiques i l'execució de noves instal·lacions.
                                        </p>
                                    </div>
                                </div><!-- end item -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section --> <!-- start section --> 
    <a id="qui_som" class="ancor mce-item-anchor"></a>
    <section class="section section--screen section--background section-about parallax" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="300" data-stellar-offset-parent="true" style="background-image: url(http://airfoc.cat/img/img_8.jpg); background-position: 50% -127.142px; background-size:auto 220%;">
        <div class="pattern" style="opacity: .5;" data-mce-style="opacity: .5;"><br></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="col-MB-30">
                        <h2 class="section-about__title">Airfoc&nbsp;30 anys fent prevenció del foc</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="col-MB-15">
                        <div class="col-MB-30">
                            <h4>Per tal d'adequar cada element extintor a la normativa vigent, s'ofereix un servei de manteniment d'extintors, instal·lacions i material de protecció contra incendis en general..</h4>
                        </div>
                        <p>AIRFOC es va fundar l'any&nbsp;1977 a Igualada (Barcelona), amb l'objectiu de ser una empresa de serveis en temes de seguretat contra incendistan a nivell industrial com particular.</p>
                        <p>
                            És una empresa dedicada a la venda, instal·lació i manteniment d'extintors, equips i sistemes de protecció contra incendis a la província de Barcelona.
                        </p>
                        <p>
                            <br>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-MB-15">
                        <p>AIRFOC és una empresa amb una llarga trajectòria en el sector, avalada amb més de 30 anys d'experiència durant els quals hem crescut i augmentat l'oferta de serveis i productes de forma sostenible, sempre mantenint la qualitat com a senyal identitària de la nostra feina.<br><br>Amb la finalitat de garantir la confiança dels nostres clients AIRFOC ha implantat un sistema de gestió amb la garantia de qualitat, conseguint el certificat UNE-EN ISO 9001, per a la istal·lació i manteniment d'aparells, equips i sistemes de protecció contra incendis.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section --> <!-- start section -->
    <section class="section" style=" background-color: #ffc107;">
        <div class="container">
            <div class="section-heading section-heading--center section-heading--white">
                <h2 class="title">Els Nostres clients diuen...</h2>
            </div>
            <div class="row flex-items-md-center">
                <div class="col-lg-10">
                    <div class="feedbacks feedbacks--slider feedbacks--style-1">
                            <div class="owl-carousel owl-theme">
                                    <!-- start item -->
                                    <article class="feedback__item">
                                            <div class="feedback__author">
                                                    <div class="feedback__author__photo  mx-auto">
                                                            <img class="circled  img-fluid" src="http://airfoc.cat//img/users_photos/1.png" height="140" width="140" alt="demo" />
                                                    </div>

                                                    <h4 class="feedback__author__name">Robert Carulla</h4>

                                                    <p class="feedback__author__position">gerent, FITREX SL</p>
                                            </div>

                                            <div class="feedback__text">
                                                    <p>
                                                            “Vam decidir treballar amb AIRFOC perquè ens donen total confiança en temes de seguretat i incendis. Un cop vam haver d'utilitzar un extintor i en menys de 24 hores ja teniem l'extintor canviat per un de nou"
                                                    </p>
                                            </div>
                                    </article>
                                    <!-- end item -->

                                    <!-- start item -->
                                    <article class="feedback__item">
                                            <div class="feedback__author">
                                                    <div class="feedback__author__photo  mx-auto">
                                                            <img class="circled  img-fluid" src="http://airfoc.cat//img/users_photos/2.png" height="140" width="140" alt="demo" />
                                                    </div>

                                                    <h4 class="feedback__author__name">Alex Roberts</h4>

                                                    <p class="feedback__author__position">director, Gràfic CTM</p>
                                            </div>

                                            <div class="feedback__text">
                                                    <p>
                                                            “Buscavem una empresa professional i que no haguessim d'estar trucant cada dos per tres per problemes. En gràfiques la seguretat i el risc d'invendis és alt i AIRFOC  ens ha donat el servei que buscavem"
                                                    </p>
                                            </div>
                                    </article>
                                    <!-- end item -->

                                    <!-- start item -->
                                    <article class="feedback__item">
                                            <div class="feedback__author">
                                                    <div class="feedback__author__photo  mx-auto">
                                                            <img class="circled  img-fluid" src="http://airfoc.cat//img/users_photos/4.png" height="140" width="140" alt="demo" />
                                                    </div>

                                                    <h4 class="feedback__author__name">Janina Orta</h4>

                                                    <p class="feedback__author__position">diseny, Sita Murt</p>
                                            </div>

                                            <div class="feedback__text">
                                                    <p>
                                                            “Són gent molt amable, professional i treballen amb productes de primera qualitat que no donen problemes. Estem molt contents amb AIRFOC"
                                                    </p>
                                            </div>
                                    </article>
                                    <!-- end item -->
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end section --> <!-- start section --> 
    <a id="efectivitat" class="ancor mce-item-anchor"></a>
    <section class="section section--screen section--background-base-light section--background-logo"><div class="container"><div class="section-heading section-heading--left"><h2 class="title">Airfoc en números</h2></div><div class="counters counters--style-2"><div class="counters__inner"><div class="row"><!-- star item --><div class="col-md-6 col-xl-3"><div class="counter__item"><i class="counter__item__ico counter__item__ico--1"></i><p class="counter__item__count js-count" data-from="0" data-to="600" data-after="">50</p><p class="counter__item__title">productes en catàleg</p></div></div><!-- end item --> <!-- star item --><div class="col-md-6 col-xl-3"><div class="counter__item"><i class="counter__item__ico counter__item__ico--2"></i><p class="counter__item__count js-count" data-from="0" data-to="50" data-after="">110</p><p class="counter__item__title">ciutats clients nostres</p></div></div><!-- end item --> <!-- star item --><div class="col-md-6 col-xl-3"><div class="counter__item"><i class="counter__item__ico counter__item__ico--3"></i><p class="counter__item__count js-count" data-from="0" data-to="250">2500</p><p class="counter__item__title">extintors instal.lats</p></div></div><!-- end item --> <!-- star item --><div class="col-md-6 col-xl-3"><div class="counter__item"><i class="counter__item__ico counter__item__ico--4"></i><p class="counter__item__count js-count" data-from="0" data-to="300" data-after="">345</p><p class="counter__item__title">número de clients Airfoc</p></div></div><!-- end item --></div></div></div></div></section><!-- end section --> <!-- start section --> 
    <a id="pressupost" class="ancor mce-item-anchor"></a>
    <section class="section section--screen section--background section-banner" style="background-image: url('../img/img_33.jpg');" data-mce-style="background-image: url('../imgimg_33.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-6">
                    <div class="col-MB-30"><h2 class="section-banner__title">Productes de qualitat</h2>
                        <h4>
                            Extintors d'altíssima qualitat, amb sistema de seguretat&nbsp;i sempre nous. No reciclem ni reomplim per evitar errors i deteriorament del material.<br>
                        </h4>
                    </div>
                    
                    <div class="item">
                        <header class="item__header">
                            <h2 class="item__title">Et fem un pressupost</h2>
                            <p class="item__subtitle " style="color: #333;" data-mce-style="color: #333;">Emplena el següent formulari i ens posarem amb contacte lo abans possible:</p>
                        </header>
                        <form onsubmit="return solicitarPresupuesto(this)">
                            <label class="input-wrp"><i class="fontello-user"></i>
                                <input class="textfield" placeholder="Nom" type="text">
                            </label>
                            <label class="input-wrp">
                                <i class="fontello-mail"></i>
                                <input class="textfield" placeholder="E-mail" type="text">
                            </label>
                            <label class="input-wrp">
                                <i class="fontello-comment"></i>
                                <textarea class="textfield" placeholder="Comentari"></textarea>
                            </label>
                            <div class="btn-wrp">
                                <button class="custom-btn long" type="submit" role="button" style="; border-color: #f6a309;color: #504935; " data-mce-style="; border-color: #f6a309; ">Enviar pressupost</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end section --> <!-- start section --> 
    <a id="productes" class="ancor mce-item-anchor"></a>
    <section class="section section--no-pt section--no-pb section-gallery">
        <!-- start item -->
        <a href="<?= site_url('p/co2') ?>">
            <div class="item" style="background-image: url('<?= base_url() ?>img/gall_img/1.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="item__title">Extintors CO2</h2>
                        </div>
                    </div>
                </div>
            </div><!-- end item --> <!-- start item -->
        </a>
        <a href="<?= site_url('p/pols') ?>">
            <div class="item" style="background-image: url('<?= base_url() ?>img/gall_img/2.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="item__title">Extintors de pols</h2>
                        </div>
                    </div>
                </div>
            </div><!-- end item --> <!-- start item -->
        </a>
        <!--<a href="<?= site_url('p/accesoris') ?>">
            <div class="item" style="background-image: url('<?= base_url() ?>img/gall_img/3.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="item__title">Accesoris per a extintors</h2>
                        </div>
                    </div>
                </div>
            </div><!-- end item --> <!-- start item -->
        <!--</a>-->
        <a href="<?= site_url('p/senalitzacio') ?>">
            <div class="item" style="background-image: url('<?= base_url() ?>img/gall_img/4.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="item__title">Señalització</h2>
                        </div>
                    </div>
                </div>
            </div><!-- end item -->
        </a>
    </section><!-- end section --> 
    <!-- start section --> 
    <a id="noticies" class="ancor mce-item-anchor"></a>
    <section class="section" style="background-color: #fafafa;" data-mce-style="background-color: #fafafa;">
        <div class="container">
            <div class="section-heading section-heading--left">
                <h2 class="title">Notícies</h2>
            </div>
            <div class="blog blog--style-3">
                <div class="blog__inner">
                    <div class="row row-no-gutter"><!-- start item -->                        
                        <?php foreach($blog->result() as $d): ?>
                            <div class="col-md-6 col-lg-4">
                                <div class="blog__item">
                                    <figure>
                                        <img src="<?= $d->foto ?>" style="background-image: url('<?= $d->foto ?>');" alt="demo">
                                    </figure>
                                    <div class="blog__entry">
                                        <span class="blog__entry__meta"><?= ucfirst(strftime("%a",strtotime($d->fecha))); ?>,<?= strftime("%d-%m-%Y",strtotime($d->fecha)); ?></span>
                                        <h3 class="blog__entry__title h4"><a href="<?= $d->link ?>"><?= $d->titulo ?></a><br></h3>
                                    </div>
                                </div>
                            </div><!-- end item --> <!-- start item -->
                        <?php endforeach ?>
                        <!-- end item -->
                    </div>
                    <div class="text-center">
                            <a id="blog-more-btn" class="custom-btn big primary" href="<?= site_url('blog') ?>">Veure Tots</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- end section --> <!-- start section -->
    
    <a id="contacte" class="ancor mce-item-anchor"></a>
    <?php $this->load->view('includes/template/contacto'); ?>
</main><!-- start footer -->
