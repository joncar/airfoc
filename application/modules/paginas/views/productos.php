<?= $this->load->view('includes/template/header') ?>
<header class="intro  flex-items-xs-middle  parallax" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="300" data-stellar-offset-parent="true" style="background-image: url(<?= base_url() ?>img/bg/bg_2.jpg);" data-mce-style="background-image: url(<?= base_url() ?>img/bg/bg_2.jpg);">
    <div class="pattern" style="opacity: 0.15;" data-mce-style="opacity: 0.15;"><br>
    </div>
    <div class="container">
        <div class="intro__text">
            <p class="intro__subtitle">Els nostres</p>
            <h1 class="intro__title">Productes</h1>
        </div>
    </div>
</header>
<main role="main"> <!-- start section -->
    <section class="section" style="background-color: #f9f9f9;" data-mce-style="background-color: #f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4 col-xl-3">
                    <div class="col-MB-30">
                        <h2>
                            Extintors <br>de CO<sub>2</sub>
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-8 col-xl-9">
                    <h4>Smells racy free announcing than durable zesty smart exotic far feel. Screamin' affordable secret way absolutely.</h4><p>Stimulates vast a real proven works discount secure care. Market invigorate a awesome handcrafted bigger comes newer recommended lifetime.</p><p>Evulates vast a real proven works discount secure care. Market invigorate a awesome handcrafted bigger comes newer recommended lifetime. Odor to yummy high racy bonus soaking mouthwatering. First superior full-bodied drink. Like outstanding odor economical deal clinically</p><p>Odor to yummy high racy bonus soaking mouthwatering. First superior full-bodied drink. Like outstanding odor economical deal clinically feel durable. Lather each real. Quite one fresh.</p><p>Delectable absorbent ordinary full-bodied out durable whopping value when. Coming supreme tropical dual locked-in sharpest effervescent zesty bigger. Opportunity and affordable clinically. Ordinary whenever appearance first first unlimited compact.</p>
                </div>
            </div>
        </div>
    </section><!-- end section --> <!-- start section -->
    <section class="section">
        <div class="container">
            <div class="products">
                <div class="products__inner">
                    <div class="row flex-items-xs-middle row-no-gutter"><!-- start item -->
                        <div class="col-md-6">
                            <div class="product__item product__item--text">
                                <i class="product__item__ico product__item__ico--1"></i>
                                <h3 class="product__item__title">Extintor 25kg CO2</h3><p style="text-align: left;" data-mce-style="text-align: left;">
                                    <strong>Especificaciones:</strong><br>
                                    <span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">
                                        -&nbsp;Presión incorporada.  
                                    </span><br>
                                    <span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">-&nbsp;Válvula de disparo rápido. </span><br><span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">
                                        - Manómetro de control instantáneo de carga.</span><br><span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">
                                            -&nbsp;Válvula de comprobación de presión interna.  </span><br><span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">
                                                -&nbsp;Manguera incorporada paa salida de producto.  </span><br><span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">
                                                    -&nbsp;&nbsp;1,2 y 3 Kg. con difusor.  </span><br><span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">
                                                        -&nbsp;Cargados con polvo A-B-C.  </span><br><span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">
                                                            -&nbsp;Gas impulsor: Nitrógeno.</span></p><p style="text-align: left;" data-mce-style="text-align: left;">
                                                                <strong>Para uso en:</strong><br><span style="font-size: 10pt;" data-mce-style="font-size: 10pt;">Locales comerciales, Industria y Transporte, Viajeros y Mercancias, Locales Públicos y diversos. Los extintores cargados con polvo químico ABC están considerados como unos de los más seguros y versátiles de los que se puede disponer en el mercado. Sus principales componentes son el Fosfato Monoamónico y el sulfato Amónico y se utilizan en la extinción de fuegos de materiales sólidos, líquidos y gaseosos. Todos los aparatos cargados con este agente extintor están homologados para su uso en vehículos según órden del M.I.E. de 30 del VII de 1975.</span></p></div>
                        </div><!-- end item --> <!-- start item -->
                        <div class="col-md-6">
                            <div class="product__item product__item--image">
                                <img class="img-fluid" src="../img/products_img/1.jpg" alt="demo">
                            </div>
                        </div><!-- end item -->
                    </div>
                    <div class="row flex-items-xs-middle row-no-gutter"><!-- start item -->
                        <div class="col-md-6">
                            <div class="product__item product__item--text">
                                <i class="product__item__ico product__item__ico--2"></i>
                                <h3 class="product__item__title">Extintor 25kg CO2</h3><p data-mce-style="text-align: left;" style="text-align: left;">
                                    <strong>Especificaciones:</strong><br>
                                    <span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                        -&nbsp;Presión incorporada.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                            -&nbsp;Válvula de disparo rápido.&nbsp;</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                                - Manómetro de control instantáneo de carga.</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                                    -&nbsp;Válvula de comprobación de presión interna.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                                        -&nbsp;Manguera incorporada paa salida de producto.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                                            -&nbsp;&nbsp;1,2 y 3 Kg. con difusor.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                                                -&nbsp;Cargados con polvo A-B-C.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">
                                                                    -&nbsp;Gas impulsor: Nitrógeno.</span></p><p data-mce-style="text-align: left;" style="text-align: left;">
                                                                        <strong>Para uso en:</strong><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">Locales comerciales, Industria y Transporte, Viajeros y Mercancias, Locales Públicos y diversos. Los extintores cargados con polvo químico ABC están considerados como unos de los más seguros y versátiles de los que se puede disponer en el mercado. Sus principales componentes son el Fosfato Monoamónico y el sulfato Amónico y se utilizan en la extinción de fuegos de materiales sólidos, líquidos y gaseosos. Todos los aparatos cargados con este agente extintor están homologados para su uso en vehículos según órden del M.I.E. de 30 del VII de 1975.</span></p>
                            </div>
                        </div><!-- end item --> <!-- end item -->
                        <div class="col-md-6">
                            <div class="product__item product__item--image">
                                <img class="img-fluid" src="../img/products_img/2.jpg" alt="demo">
                            </div>
                        </div><!-- end item -->
                    </div>
                    <div class="row flex-items-xs-middle row-no-gutter"><!-- start item -->
                        <div class="col-md-6">
                            <div class="product__item product__item--text">
                                <i class="product__item__ico product__item__ico--3"></i>
                                <h3 class="product__item__title">Extintor 25kg CO2</h3>
                                <p data-mce-style="text-align: left;" style="text-align: left;"><strong>Especificaciones:</strong><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Presión incorporada.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Válvula de disparo rápido.&nbsp;</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">- Manómetro de control instantáneo de carga.</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Válvula de comprobación de presión interna.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Manguera incorporada paa salida de producto.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;&nbsp;1,2 y 3 Kg. con difusor.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Cargados con polvo A-B-C.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Gas impulsor: Nitrógeno.</span></p><p data-mce-style="text-align: left;" style="text-align: left;"><strong>Para uso en:</strong><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">Locales comerciales, Industria y Transporte, Viajeros y Mercancias, Locales Públicos y diversos. Los extintores cargados con polvo químico ABC están considerados como unos de los más seguros y versátiles de los que se puede disponer en el mercado. Sus principales componentes son el Fosfato Monoamónico y el sulfato Amónico y se utilizan en la extinción de fuegos de materiales sólidos, líquidos y gaseosos. Todos los aparatos cargados con este agente extintor están homologados para su uso en vehículos según órden del M.I.E. de 30 del VII de 1975.</span></p>
                            </div>
                        </div><!-- start item -->
                        <div class="col-md-6">
                            <div class="product__item product__item--image"><img class="img-fluid" src="../img/products_img/3.jpg" alt="demo"></div></div>
                        <!-- end item -->
                    </div>
                    <div class="row flex-items-xs-middle row-no-gutter"><!-- start item --><div class="col-md-6"><div class="product__item product__item--text"><i class="product__item__ico product__item__ico--4"></i><h3 class="product__item__title">Extintor 25kg CO2</h3><p data-mce-style="text-align: left;" style="text-align: left;"><strong>Especificaciones:</strong><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Presión incorporada.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Válvula de disparo rápido.&nbsp;</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">- Manómetro de control instantáneo de carga.</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Válvula de comprobación de presión interna.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Manguera incorporada paa salida de producto.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;&nbsp;1,2 y 3 Kg. con difusor.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Cargados con polvo A-B-C.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Gas impulsor: Nitrógeno.</span></p><p data-mce-style="text-align: left;" style="text-align: left;"><strong>Para uso en:</strong><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">Locales comerciales, Industria y Transporte, Viajeros y Mercancias, Locales Públicos y diversos. Los extintores cargados con polvo químico ABC están considerados como unos de los más seguros y versátiles de los que se puede disponer en el mercado. Sus principales componentes son el Fosfato Monoamónico y el sulfato Amónico y se utilizan en la extinción de fuegos de materiales sólidos, líquidos y gaseosos. Todos los aparatos cargados con este agente extintor están homologados para su uso en vehículos según órden del M.I.E. de 30 del VII de 1975.</span></p></div></div><!-- end item --> <!-- start item --><div class="col-md-6"><div class="product__item product__item--image"><img class="img-fluid" src="../img/products_img/4.jpg" alt="demo"></div></div><!-- end item --></div><div class="row flex-items-xs-middle row-no-gutter"><!-- start item --><div class="col-md-6"><div class="product__item product__item--text"><i class="product__item__ico product__item__ico--5"></i><h3 class="product__item__title">Extintor 25kg CO2</h3><p data-mce-style="text-align: left;" style="text-align: left;"><strong>Especificaciones:</strong><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Presión incorporada.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Válvula de disparo rápido.&nbsp;</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">- Manómetro de control instantáneo de carga.</span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Válvula de comprobación de presión interna.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Manguera incorporada paa salida de producto.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;&nbsp;1,2 y 3 Kg. con difusor.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Cargados con polvo A-B-C.  </span><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">-&nbsp;Gas impulsor: Nitrógeno.</span></p><p data-mce-style="text-align: left;" style="text-align: left;"><strong>Para uso en:</strong><br><span data-mce-style="font-size: 10pt;" style="font-size: 10pt;">Locales comerciales, Industria y Transporte, Viajeros y Mercancias, Locales Públicos y diversos. Los extintores cargados con polvo químico ABC están considerados como unos de los más seguros y versátiles de los que se puede disponer en el mercado. Sus principales componentes son el Fosfato Monoamónico y el sulfato Amónico y se utilizan en la extinción de fuegos de materiales sólidos, líquidos y gaseosos. Todos los aparatos cargados con este agente extintor están homologados para su uso en vehículos según órden del M.I.E. de 30 del VII de 1975.</span></p></div></div><!-- end item --> <!-- start item --><div class="col-md-6"><div class="product__item product__item--image"><img class="img-fluid" src="../img/products_img/5.jpg" alt="demo"></div></div><!-- end item --></div></div></div></div>
    </section><!-- end section --> <!-- start section --> 
    <a id="spy-offer" class="ancor mce-item-anchor"></a>
    <section class="section section--screen section--background section-banner" style="
background-image: url('../img/img_33.jpg');
    min-height: -webkit-calc(100vh - 80px);
    min-height: calc(40vh - 80px);" data-mce-style="background-image: url('../imgimg_33.jpg');min-height: -webkit-calc(60vh - 80px);
min-height: calc(40vh - 80px);"></section><!-- end section --> </main>