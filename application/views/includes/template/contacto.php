<section class="section-contact">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="item map-container" style="background: url(<?= base_url() ?>img/footer.jpg) no-repeat; background-size:cover;">
                    
                </div>
            </div>
            <div class="col-xs-12 col-md-6 bg-2">
                <div class="item">
                    <header class="item__header">
                        <h2 class="item__title">Contacte amb nosaltres</h2>
                        <p class="item__subtitle">Emplena el següent formulari i ens posarem amb contacte lo abans possible:</p>
                    </header>
                    <form onsubmit="return contacto(this)">
                        <label class="input-wrp"><i class="fontello-user"></i>
                            <input class="textfield" name='nombre' placeholder="Nom" type="text">
                        </label>
                        <label class="input-wrp"><i class="fontello-mail"></i>
                            <input class="textfield" name="email" placeholder="E-mail" type="text">
                        </label>
                        <label class="input-wrp"><i class="fontello-comment"></i>
                            <textarea class="textfield" name="comentario" placeholder="Comentari"></textarea>
                        </label>
                        <div class="btn-wrp">
                            <button class="custom-btn long" id='guardar' type="submit" role="button">Enviar missatge</button>
                        </div>
                        <div class="btn-wrp" id='message'>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section><!-- end section --> 