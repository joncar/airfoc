<!-- login popup-->
    <div class="login-popup">
      <div class="login-popup-wrap">
        <div class="title-wrap">
          <h2>Login</h2><i class="close-button flaticon-close"></i>
        </div>
        <div class="login-content">
          <form class="form">
            <input type="text" name="email" value="" size="40" placeholder="Enter Your Email ..." aria-required="true" class="form-row form-row-first">
            <input type="text" name="password" value="" size="40" placeholder="Enter Your Password ..." aria-required="true" class="form-row form-row-last">
          </form>
          <div class="remember">
            <div class="checkbox">
              <input id="checkbox30" type="checkbox" value="None" name="check">
              <label for="checkbox30">Remember Me</label>
            </div><a href="#">Forgot Password ?</a>
          </div><a href="#" class="cws-button gray alt full-width mt-20">Login now</a>
        </div>
        <div class="login-bot">
          <p>No account yet? <a href="#">Register now</a></p>
        </div>
      </div>
    </div>
    <!-- ! login popup-->
    <!-- news popup-->
    <div class="news-popup"> 
      <div class="news-popup-wrap"> <i class="close-button flaticon-close"></i>
        <div class="row">
          <div class="col-sm-6"><img src="pic/news-popup.jpg" data-at2x="pic/news-popup@2x.jpg" alt></div>
          <div class="col-sm-6">
            <div class="news-content">
              <div class="news-title">
                <h2>Newsletter</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
              </div>
              <form method="get" action="#" class="newsletter contact-form">
                <label class="mb-0">
                  <input type="text" placeholder="Enter Your Email ..." value="" name="email" class="newsletter-field mb-0">
                </label>
                <button type="submit" class="newsletter-submit cws-button alt">Submit</button>
              </form>
              <div class="checkbox-wrap">
                <div class="checkbox">
                  <input id="checkbox40" type="checkbox" value="None" name="check">
                  <label for="checkbox40">Dont Show This Message Again</label>
                </div>
              </div>
              <div class="social-wrap"><a href="#" class="cws-social flaticon-social-4"></a><a href="#" class="cws-social flaticon-social"></a><a href="#" class="cws-social flaticon-social-3"></a><a href="#" class="cws-social flaticon-social-1"></a><a href="#" class="cws-social flaticon-social-network"></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>    