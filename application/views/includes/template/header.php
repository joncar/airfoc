<!-- start top bar -->
<div id="top-bar" class="top-bar--style-3">
    <div class="container">
        <a id="top-bar__logo" class="site-logo" href="<?= site_url() ?>">AIRFOC</a>
        <a id="top-bar__navigation-toggler" class="mce-item-anchor"><span></span></a>
        <nav id="top-bar__navigation" role="navigation">
            <ul class="nav">                
                    <li><a href="<?= site_url() ?>#serveis">SERVEIS</a><br></li>
                    <li><a href="<?= site_url() ?>#qui_som">qui som?</a><br></li>
                    <li><a href="<?= site_url() ?>#efectivitat">EFECTIVITAT</a><br></li>
                    <li><a href="<?= site_url() ?>#pressupost">PRESSUPOST</a><br></li>
                    <li>
                        <a href="<?= site_url() ?>#productes">PRODUCTES</a>
                            <div class="submenu">
                                    <ul>
                                            <li><a href="<?= site_url('p/co2') ?>">Extintors C02</a><br></li>
                                            <li><a href="<?= site_url('p/pols') ?>">Extintors de pols</a><br></li>
                                            <!--<li><a href="<?= site_url('p/accesoris') ?>">Accesoris</a><br></li>-->
                                            <li><a href="<?= site_url('p/senalitzacio') ?>">Señalització</a><br></li>
                                            <li><a href="<?= site_url('p/avis-legal') ?>">Avís legal</a><br></li>
                                    </ul>
                            </div>
                    </li>
                    <li><a href="<?= site_url() ?>#noticies">NOTÍCIES</a><br></li>
                    <li class="li-btn"><a class="custom-btn primary" href="<?= site_url() ?>#contacte">Contacte</a><br></li>
            </ul>
        </nav>
    </div>
</div><!-- end top bar --><!-- start header -->
