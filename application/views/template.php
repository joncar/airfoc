<!DOCTYPE html>
<html lang="en-us" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">          
        <title><?= empty($title)?'Airfoc':$title ?></title>
        <link rel="shortcut icon" href="img/favicon.ico">
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/style.css" type="text/css">
        <script type="text/javascript">
                WebFontConfig = {
                        google: { families: [ 'Poppins:300,400,500,600,700', 'Raleway:400,400i,500,500i,700,700i'] }
                };
                (function() {
                        var wf = document.createElement('script');
                        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + 
                        '://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
                        wf.type = 'text/javascript';
                        wf.async = 'true';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(wf, s);
                })();
        </script>
        <script type="text/javascript">
                var _html = document.documentElement;
                _html.className = _html.className.replace("no-js","js");
        </script>
        <script type="text/javascript" src="<?= base_url() ?>js/template/device.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/frame.js"></script>
    </head>
    <body class="page page-landing">
        <?= $this->load->view($view) ?>
        <?= $this->load->view('includes/template/footer') ?>
        <?php if(empty($scripts)){$this->load->view('includes/template/scripts');} ?>
        <?php $this->load->view('predesign/cookies'); ?>      
    </body>   
</html>